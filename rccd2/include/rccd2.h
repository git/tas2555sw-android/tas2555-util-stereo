#ifndef RCCD2_H_
#define RCCD2_H_

#define FW_ID_ADDRESS   0x1FBC

#define TARGET_I2C          5
#define RCCD_MAGIC          "TICC"
#define RCCD_PING           "PING"
#define DEFAULT_FWID        "FWIDTLV320AIC3262EVM-U"    
#define RCCD_MAX_PACKET     (32*1024)
#define RCCD_MAX_STREAMS    (8)

enum __attribute__ ((__packed__)) rccd_cmd_id {
    RCCD_CMD_WRITE      = 0x02,
    RCCD_CMD_READ       = 0x03,
    RCCD_CMD_FWID       = 0x05,
    RCCD_CMD_SLAVE      = 0x06,

    RCCD_CMD_BLOCK      = 0x80,
    RCCD_CMD_OPEN,
    RCCD_CMD_CLOSE,
    RCCD_CMD_PLAY,
    RCCD_CMD_CAPTURE,
    RCCD_CMD_START,
    RCCD_CMD_STOP,
    RCCD_CMD_MGET,
    RCCD_CMD_MSET,
    RCCD_CMD_NCTLS,
    RCCD_CMD_CTL
};

struct rccd_cmd {
    u8 len;
    enum rccd_cmd_id cid;
    u8 options;
    u8 data[];
};
struct rccd_cmd_alsa {
    u8 len;
    enum rccd_cmd_id cid;
    u8 stream;
    u8 device;
    unsigned int flags;
    unsigned int buffer_size;
    unsigned int channels;
    unsigned int rate;
    unsigned int period_size;
    unsigned int period_count;
    unsigned int format;
    u8 data[];
};

struct rccd_request {
    u8 magic[4];
    u8 len;
    u8 app;
    u8 target;
    u8 result;
    struct rccd_cmd cmd[];
};

#endif
