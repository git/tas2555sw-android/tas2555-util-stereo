#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <getopt.h>

#include "host_compile.h"

extern int verbose;
void error(char *fmt, ...);
void warn(char *fmt, ...);
void dbg(const char *f, int ln, char *fmt, ...);

#define warn_once(f, ...) do {                              \
                                static int prn_out__ = 0;   \
                                if (!prn_out__)  {          \
                                    warn(f, ##__VA_ARGS__); \
                                    prn_out__ = 1;          \
                                }                           \
                            } while (0)

    
#define DBG(f, ...) dbg(__FILE__, __LINE__, f, ##__VA_ARGS__)
#define DBGF(f, ...) dbg(__FUNCTION__, __LINE__, f, ##__VA_ARGS__)

#define ARRAY_LEN(x) ((int)(sizeof(x)/sizeof((x)[0])))
#define MAX2(x, y)(((x)>(y))?(x):(y))
#define MIN2(x, y)(((x)<(y))?(x):(y))


#ifdef WIN32
char *strcasestr(const char *haystack, const char *needle);
#endif

#define ALLOC(x) safe_alloc(__FUNCTION__, __FILE__, __LINE__, x)
#define REALLOC(x, n) do { \
        (x) = realloc((x), n); \
        if (!(x)) { \
            error("out of memory: request for %d bytes in %s() [%s:%d]", \
                n, __FUNCTION__, __FILE__, __LINE__); \
        } \
    } while (0)

void *safe_alloc(const char *fn, const char *file,  int ln, int n);
void safe_snprintf(char *str, size_t size, const char *format, ...);

int flt2fix(char *s);
void *slurp(const char *f, int *np);

char *make_opts(const struct option *opts);
char *change_extn(const char *fname, char *ext);
char *get_extn(const char *fname);
double clkspec(const char *mclk);

void add_path(char *path);
FILE *loc_and_open(char *file);
int file_exists(const char *path);

int split(const char *s, char ***flds);
void free_flds(char **flds, int n);


#ifndef __ARMEL__
// No regexp library installed for Linux/Android
//int grok_reg_spec(const char *exp, union aic_register *r);
#endif

#endif
