#ifndef HOST_COMPILE_H_
#define HOST_COMPILE_H_

#define printk warn
#define MODULE_DESCRIPTION(x) 
#define MODULE_LICENSE(x) 
#define MODULE_AUTHOR(x) 

//#include <asm-generic/int-ll64.h>
typedef unsigned char u8;
typedef unsigned short int u16;
//typedef unsigned long int u32;
typedef unsigned int u32;
typedef signed char i8;
typedef signed short int i16;
typedef signed long int i32;
//#define crc32  aic_crc32
struct aic_firmware *aic_unpickle(void *pcfw, int n);
unsigned int crc32_local(unsigned int *pdata, int n);
#if 0
static inline unsigned int crc32(unsigned int seed, unsigned int *pdata, int n)
{
    return  crc32_local(pdata, n);
}
#endif
#   define _GNU_SOURCE
#   include <stdlib.h>
#   include "utils.h"
#   include <string.h>
#   include <assert.h>
#ifndef EINVAL
#   define EINVAL 1
#endif

// Hack, Hack
#ifndef AIC3XX_COPS_MDSP_D
#define AIC3XX_COPS_MDSP_D  (0x00000003u)
#define AIC3XX_COPS_MDSP_A  (0x00000030u)
#endif

#endif
