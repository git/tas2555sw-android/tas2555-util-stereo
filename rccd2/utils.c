#define _GNU_SOURCE
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "utils.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int verbose = 1;
void error(char *fmt, ...)
{
    va_list ap;
    fprintf(stderr, "error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    exit(2);
}
void warn(char *fmt, ...)
{
    va_list ap;
    if (verbose <= 0)
        return;
    fprintf(stderr, "warning: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
}
void dbg(const char *f, int ln, char *fmt, ...)
{
    va_list ap;
    if (getenv("MKCFW_VERBOSITY"))
        verbose = atoi(getenv("MKCFW_VERBOSITY"));
    if (verbose <= 1)
        return;
    fprintf(stderr, "DBG[%16s:%3d]: ", f, ln);
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
}
void *safe_alloc(const char *fn, const char *file,  int ln, int n)
{
    void *ret = calloc(n, 1);
    if (!ret)
        error("out of memory: request for %d bytes from function %s() [%s:%d]",
                n, fn, file, ln);
    return ret;
}
void safe_snprintf(char *str, size_t size, const char *format, ...)
{
    char *t = ALLOC(size);
    va_list ap;
    va_start(ap, format);
    vsnprintf(t, size, format, ap);
    va_end(ap);
    strcpy(str, t);
    free(t);
}
char *make_opts(const struct option *options)
{
    int i = 0;
    static char opts[256];
    while (options[i].name) {
        safe_snprintf(opts, sizeof(opts), "%s%c%s%s", opts, options[i].val,
                options[i].has_arg?":":"",
                (options[i].has_arg == 2)?":":"");
        ++i;
    }
    return opts;
}
