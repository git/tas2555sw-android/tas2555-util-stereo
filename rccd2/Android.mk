LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= external/rccd2/include
LOCAL_CFLAGS := -O2 -g -W -Wall -D_LARGEFILE_SOURCE -DAIC3XXX_CFW_HOST_BLD -D_FILE_OFFSET_BITS=64
LOCAL_SRC_FILES:= rccd2.c android_utils.c utils.c
LOCAL_MODULE := rccd2
LOCAL_STATIC_LIBRARIES:= libcutils libutils liblog
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS += -Werror

include $(BUILD_EXECUTABLE)
