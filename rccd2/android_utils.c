#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/kdev_t.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include "utils.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include "aic3xxx_cfw.h"
#include "utils.h"
#include "host_compile.h"
#include "android_utils.h"

int dump_cmds = 0;
int mode = mode_undef;

static int dev_fd, dev_no, modern;

#define DEVICE_ID 98

int regDumpId;
char regBuffer[512];

//static char *device;
void diag(int lvl, char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
#ifdef ANDROID
	__android_log_vprint(lvl, PROGRAM, fmt, ap);
#endif
	va_end(ap);
	if (getppid() != 1 && (lvl >= (ANDROID_LOG_ERROR - verbose))) {
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		va_end(ap);
		fprintf(stderr, "\n");
	}
	if (lvl >= ANDROID_LOG_FATAL)
		exit(1);
}

const struct { char *node; int cfwb; } devices[] = {
	{ "aic3xxx_cfw", 1 },
	{ "tiload", 0 },
	{ "tiload_node", 0 }
};
int open_device(char *device)
{
	char dfile[64];
	struct stat sbuf;
	int i = 0, fd;
	if (device)  {
		// Possible use on host
		DBGW("Device not null");
		if (stat(device, &sbuf) == 0) {
			strncpy(dfile, device, sizeof(dfile)-1);
			goto open;
		}
		DBGW("Could not locate device \"%s\"", device);
		// Switch exclusive to old style device if one is specified (rccd backwards-compatibilty)
		while (devices[i].cfwb)
			++i;
	}
	for (; i < ARRAY_LEN(devices); ++i) {
		sprintf(dfile, "/dev/%s", devices[i].node);
		if (stat(dfile, &sbuf) == 0)
			goto open;
		DBGW("Missing device node \"%s\"\n\tLooking for %s in /proc/devices...", dfile, devices[i].node);
		FILE *fp = fopen("/proc/devices", "r");
		if (!fp)
			DBGF("Unable to read /proc/devices");
		char ln[4096];
		while (fgets(ln, sizeof(ln), fp)) {
			if (!strstr(ln, devices[i].node))
				continue;
			ln[strlen(ln) - 1] = 0;
			DBGW("Found node entry \"%s\"", ln);
			int major = strtol(ln, NULL, 10);
			DBGW("\tMajor number = %d", major);
			if (mknod(dfile, S_IFCHR | 0777, MKDEV(major, 0)) < 0)
				DBGF("Unable to\n\tmknod \"%s\" c %d 0", dfile, major);
			goto open;
		}
	}
	//This was DBGF  
	DBGW("Unable find entry for codec diagnostic device.  Please check\n"
		"\tcat /proc/devices");


	// CUstom code 
	//dfile = "/sdcard/RCCD2/Test.dev";
	sprintf(dfile, "/sdcard/RCCD2/Test.dev");
	i = 4;
	DBGW("Device open is redirected to a file now - /sdcard/RCCD2/Test.dev");
	goto open;


	return -1;
open:
	if ((fd = open(dfile, O_RDWR, 0)) < 0)
		DBGF("Unable to open device %s", dfile);
	modern = devices[i].cfwb;
	if (!modern) {
		// This stuff below is needed on the old tiload device 
		// for purposes I don't fully understand
		char magic[4];
		ioctl(fd, _IOR(0xE0, 3, int), magic);
	}
	dev_fd = fd;
	dev_no = i;
	if (mode == mode_undef)
		mode = devices[i].cfwb ? mode_kernel : mode_user;
	DBG("Opened device %s, mode:%s", dfile, modern ? "modern" : "legacy");
	return fd;
}

static int cbook = -1, cpage = -1, lock = 0;


static void  mread_reg(union cfw_register reg, u8 *buf, int n)
{
	int ncmds = CFW_CMD_BURST_LEN(n);
	int err, sz = CFW_BLOCK_SIZE(ncmds);
	struct cfw_block *blk = ALLOC(sz);
	blk->ncmds = ncmds;
	blk->cmd[0].bhdr.cid = CFW_CMD_RBURST;
	blk->cmd[0].bhdr.len = n;
	blk->cmd[1].reg = reg;
	if ((err = read(dev_fd, blk, sz)) != sz)
		DBGF("Device read error %d", err);
	memcpy(buf, blk->cmd[1].burst.data, n);
}

static void  mwrite_reg(union cfw_register reg, const u8 *buf, int n)
{
	int ncmds = (n <= 3) ? n : CFW_CMD_BURST_LEN(n);
	int i, err, sz = CFW_BLOCK_SIZE(ncmds);
	struct cfw_block *blk = ALLOC(sz);
	blk->ncmds = ncmds;
	if (n <= 3) {
		for (i = 0; i < n; ++i) {
			blk->cmd[i].reg = reg;
			blk->cmd[i].reg.offset = reg.offset + i;
			blk->cmd[i].reg.data = buf[i + 1];
		}
	}
	else {
		blk->cmd[0].bhdr.cid = CFW_CMD_BURST;
		blk->cmd[0].bhdr.len = n;
		blk->cmd[1].reg = reg;
		memcpy(blk->cmd[1].burst.data, buf + 1, n);
	}
	if ((err = write(dev_fd, blk, sz)) != sz)
		DBGF("Device write error %d", err);
}


#define DREAD(reg, b, n) do { \
	int err_ret__;                  \
	DBGI("r 0x%02x 0x%02x, modern:%d", (b)[0], (n), modern); \
	sprintf(regBuffer, "\nr %d %02x %02x%c", DEVICE_ID, (b)[0], (n),'\0'); \
	write(regDumpId, &regBuffer, strlen(regBuffer)); \
if (modern) {\
	mread_reg(reg, (b), n); \
}else if ((err_ret__ = read(dev_fd, b, n)) != n) \
	DBGF("Device read error %d", err_ret__); \
} while (0)
#define DWRITE(reg, b, n) do { \
	int err_ret__, i__;                  \
	DBGI("w 0x%02x 0x%02x", (b)[0], (b)[1]); \
	sprintf(regBuffer, "\nw %d %02x %02x%c", DEVICE_ID, (b)[0], (b)[1], '\0'); \
	write(regDumpId, &regBuffer, strlen(regBuffer)); \
for (i__ = 2; i__ < n; ++i__) \
{DBGI("> 0x%02x", (b)[i__]); \
	sprintf(regBuffer, " %02x%c", (b)[i__],'\0'); \
	write(regDumpId, &regBuffer, strlen(regBuffer)); }\
if (modern) {\
	mwrite_reg(reg, b, n); \
}else if ((err_ret__ = write(dev_fd, b, n)) != n) \
	DBGF("Device write error %d", err_ret__); \
} while (0)

static void cfw_lock(int l)
{
	if (l && !lock) {
		// FIXME dummy
		lock = 1;
	}
	else if (!l && lock) {
		lock = 0;
	}
}
static int restore_lock;
static void setpb(union cfw_register r)
{
	DBG("Enter setpb");
	u8 data[2];
	restore_lock = lock;
	cfw_lock(1);
	DBG("Set Page/Book: (0x%02x,0x%02x)", r.book, r.page);
	// if (cbook != r.book) {
	if (1){
		//if (cpage != 0) {  
		if (1){
			data[0] = data[1] = 0;
			if (!modern)
				DWRITE(r, data, 2);
			cpage = 0;
		}
		data[0] = 0x7F; data[1] = r.book;
		if (!modern)
			DWRITE(r, data, 2);
		cbook = r.book;
		DBG("Set Book: [0x%02x]", r.book);
	}

	//if (cpage != r.page) {
	if (1){ 
		data[0] = 0; data[1] = r.page;
		if (!modern)
			DWRITE(r, data, 2);
		cpage = r.page;
		DBG("Set Page: [0x%02x]", r.page);
	}
	DBG("Exit setpb");
}
static void unsetpb(void)
{
	DBG("Enter unsetpb");
	cfw_lock(restore_lock);
	DBG("Exit unsetpb");
}

static u8 reg_read(union cfw_register reg)
{
	u8 ret;
	ret = reg.offset;
	setpb(reg);
	mread_reg(reg, &ret, 1);

	DREAD(reg, &ret, 1);
	unsetpb();
	return ret;
}
static void reg_write(union cfw_register reg)
{
	u8 data[2];
	data[0] = reg.offset;
	data[1] = reg.data;
	setpb(reg);
	DWRITE(reg, data, 2);
	unsetpb();
}
static void reg_wait(union cfw_register reg, u8 mask, u8 data)
{
	u8 cur = reg_read(reg);
	while ((cur & mask) != data) {
		usleep(2000);
		cur = reg_read(reg);
	}
}
static int set_bits(u8 *data, u8 mask, u8 val)
{
	u8 nd;
	nd = ((*data&(~mask)) | (mask&val));
	if (nd == *data)
		return 0;
	*data = nd;
	return 1;
}
static void cfw_op(unsigned char *var, struct cfw_cmd_op cmd)
{
	u32 op1, op2;
	u32 cid = cmd.cid;

	op1 = cmd.op1;
	op2 = cmd.op2;
	if (cid&CFW_CMD_OP1_ID)
		op1 = var[op1];
	if (cid&CFW_CMD_OP2_ID)
		op2 = var[op2];
	cid &= ~(CFW_CMD_OP1_ID | CFW_CMD_OP2_ID);

	switch (cid) {
	case CFW_CMD_OP_ADD: var[cmd.dst] = op1 + op2; break;
	case CFW_CMD_OP_SUB: var[cmd.dst] = op1 - op2; break;
	case CFW_CMD_OP_MUL: var[cmd.dst] = op1*op2; break;
	case CFW_CMD_OP_DIV: var[cmd.dst] = op1 / op2; break;
	case CFW_CMD_OP_AND: var[cmd.dst] = op1 & op2; break;
	case CFW_CMD_OP_OR: var[cmd.dst] = op1 | op2; break;
	case CFW_CMD_OP_SHL: var[cmd.dst] = (op1 << op2); break;
	case CFW_CMD_OP_SHR: var[cmd.dst] = (op1 >> op2); break;
	case CFW_CMD_OP_RR:
		while (op2--) {
			var[cmd.dst] = (op1 >> 1) | ((op1 & 1) << 7);
		}
		break;
	case CFW_CMD_OP_XOR: var[cmd.dst] = op1 ^ op2; break;
	case CFW_CMD_OP_NOT: var[cmd.dst] = ~op1; break;
	case CFW_CMD_OP_LNOT: var[cmd.dst] = !op1; break;
	default: break;
	}
}

static int bulk_write(union cfw_register reg, int count, const u8 *buf)
{
	setpb(reg);
	DWRITE(reg, buf - 1, count + 1);
	unsetpb();
	return 0;
}

static int bulk_read(union cfw_register reg, int count, u8 *buf)
{
	int i;
	if (reg.offset != buf[-1]) {
		DBGW("Mismatched register offset for read (%d != %d)",
			reg.offset, buf[-1]);
	}
	buf[0] = reg.offset;

	if (reg.offset == 0xFF && reg.book == 0xFF && reg.page == 0xFF)
	{
		/* To handle Ping*/
		buf[0] = 0x7c;
		buf[1] = 00;
		buf[2] = 01;
		buf[3] = 02;

		DBGI("..........PING.......");

	}
	else
	{

		setpb(reg);
		//DBG("Commenting of DRED");
		DREAD(reg, buf, count);
		unsetpb();
	}
	DBGI("Reading from (%d,%d,%d)... %d bytes", reg.book, reg.page, reg.offset,
		count);
	for (i = 0; i < count; i += 8) {
		switch (count - i) {
		case 1: DBGI("# 0x%02x", buf[i + 0]); break;
		case 2: DBGI("# 0x%02x 0x%02x", buf[i + 0], buf[i + 1]); break;
		case 3: DBGI("# 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2]); break;
		case 4: DBGI("# 0x%02x 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3]); break;
		case 5: DBGI("# 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3], buf[i + 4]); break;
		case 6: DBGI("# 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3], buf[i + 4], buf[i + 5]); break;
		case 7: DBGI("# 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3], buf[i + 4], buf[i + 5], buf[i + 6]); break;
		default:DBGI("# 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3], buf[i + 4], buf[i + 5], buf[i + 6], buf[i + 7]); break;
		}
	}


	return 0;
}

static void run_block_slow(struct cfw_block *pb, u8 *var)
{
	int pc = 0;
	int cond = 0;
	u8 data;
//	int i;
	
	DBG("%s: pb->ncmds=%d", __FUNCTION__, pb->ncmds);
	while (pc < pb->ncmds) {
		union cfw_cmd *c = &(pb->cmd[pc]);
		if (c->cid != CFW_CMD_BRANCH_IM &&
			c->cid != CFW_CMD_BRANCH_ID &&
			c->cid != CFW_CMD_NOP)
			cond = 0;
		DBGI("%s: Case No %d:", __FUNCTION__, c->cid);
		switch (c->cid) {

		case 0 ... (CFW_CMD_NOP - 1) :
			DBGI("CFW_CMD_NOP-1");
			reg_write(c->reg);
			pc += 1;
			break;
		case CFW_CMD_NOP:
			DBGI("CFW_CMD_NOP");
			pc += 1;
			break;
		case CFW_CMD_DELAY:
			DBGI("CFW_CMD_DELAY");
			usleep(c->delay.delay * 1000 + c->delay.delay_fine * 4);
			pc += 1;
			break;
		case CFW_CMD_UPDTBITS:
			DBGI("CFW_CMD_UPDTBITS");
			c[1].reg.data = reg_read(c[1].reg);
			set_bits(&data, c->bitop.mask, c[1].reg.data);
			reg_write(c[1].reg);
			pc += 2;
			break;
		case CFW_CMD_WAITBITS:
			DBGI("CFW_CMD_WAITBITS");
			reg_wait(c[1].reg, c->bitop.mask, c[1].reg.data);
			pc += 2;
			break;
		case CFW_CMD_LOCK:
			DBGI("CFW_CMD_LOCK");
			cfw_lock(c->lock.lock);
			pc += 1;
			break;
		case CFW_CMD_BURST:
			DBGI("CFW_CMD_BURST");
			bulk_write(c[1].reg, c->bhdr.len,
				c[1].burst.data);
			pc += CFW_CMD_BURST_LEN(c->bhdr.len);
			break;
		case CFW_CMD_RBURST:
			DBGI("CFW_CMD_RBURST");
			bulk_read(c[1].reg, c->bhdr.len,
				c[1].burst.data);
			pc += CFW_CMD_BURST_LEN(c->bhdr.len);
			break;
		case CFW_CMD_LOAD_VAR_IM:
			set_bits(&var[c->ldst.dvar],
				c->ldst.mask, c->ldst.svar);
			pc += 1;
			break;
		case CFW_CMD_LOAD_VAR_ID:
			if (c->ldst.svar != c->ldst.dvar) {
				set_bits(&var[c->ldst.dvar],
					c->ldst.mask, var[c->ldst.svar]);
				pc += 1;
			}
			else {
				data = reg_read(c[1].reg);
				set_bits(&var[c->ldst.dvar],
					c->ldst.mask, data);
				pc += 2;
			}
			break;
		case CFW_CMD_STORE_VAR:
			if (c->ldst.svar != c->ldst.dvar) {
				c[1].reg.data = reg_read(c[1].reg);
				set_bits(&(c[1].reg.data),
					var[c->ldst.dvar],
					var[c->ldst.svar]);
				reg_write(c[1].reg);
			}
			else {
				c[1].reg.data = reg_read(c[1].reg);
				set_bits(&data, c->ldst.mask,
					var[c->ldst.svar]);
				reg_write(c[1].reg);
			}
			pc += 2;
			break;
		case CFW_CMD_COND:
			cond = var[c->cond.svar] & c->cond.mask;
			pc += 1;
			break;
		case CFW_CMD_BRANCH:
			pc = c->branch.address;
			break;
		case CFW_CMD_BRANCH_IM:
			if (c->branch.match == cond)
				pc = c->branch.address;
			else
				pc += 1;
			break;
		case CFW_CMD_BRANCH_ID:
			if (var[c->branch.match] == cond)
				pc = c->branch.address;
			else
				pc += 1;
			break;
		case CFW_CMD_PRINT:
		{
							  union cfw_cmd *parglist = c + CFW_CMD_PRINT_ARG(c->print);
#ifdef ANDROID
							  DBGI(c->print.fmt,
								  var[parglist->print_arg[0]],
								  var[parglist->print_arg[1]],
								  var[parglist->print_arg[2]],
								  var[parglist->print_arg[3]]);
#else
							  printf(c->print.fmt,
								  var[parglist->print_arg[0]],
								  var[parglist->print_arg[1]],
								  var[parglist->print_arg[2]],
								  var[parglist->print_arg[3]]);
#endif
							  pc += CFW_CMD_PRINT_LEN(c->print);
		}
			break;
		case CFW_CMD_OP_START ... CFW_CMD_OP_END:
			cfw_op(var, c->op);
			pc += 1;
			break;
		default:
			DBGW("Unknown cmd command %x. Skipped", c->cid);
			pc += 1;
			break;
		}
	}

}

void run_block(struct cfw_block *pb, u8 *var)
{
	char *filePath = "/sdcard/RCCD2/regDump.txt";
	if (!modern || (mode != mode_kernel)) {
		DBGI("Calling run block slow");
		regDumpId = open(filePath, O_RDWR | O_APPEND | O_CREAT, 0644);
		run_block_slow(pb, var);
		close(regDumpId);
		return;
	}
	int r, n = CFW_BLOCK_SIZE(pb->ncmds);
	DBGI("Calling write, ncmds %d, len %d, %x, size of union cfw_cmd %d ", n, CFW_BLOCK_SIZE(pb->ncmds - 1), pb->cmd[0].reg.data, sizeof(union cfw_cmd));
	if ((r = write(dev_fd, pb, n)) != n)
		DBGF("Device write error %d", r);

}
